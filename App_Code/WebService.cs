﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for WebService
/// </summary>
//[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string DisplayShows()
    {
        using (WebClient wc = new WebClient())
        {
            string json = wc.DownloadString("https://raw.githubusercontent.com/mi9/coding-challenge-samples/master/sample_request.json");
            JObject results = JObject.Parse(json);

            var jsonObject = new JObject();

            dynamic payload = jsonObject;

            payload.response = new JArray() as dynamic;

            dynamic show = new JObject();

            foreach (var result in results["payload"])
            {
                if ((string)result["drm"] == "True" && Int32.Parse((string)result["episodeCount"]) > 0)
                {
                    show.image = (string)result["image"]["showImage"];
                    show.slug = (string)result["slug"];
                    show.title = (string)result["title"];
                    payload.response.Add(show);
                }
            }
           return payload.ToString();
        }
    }

}